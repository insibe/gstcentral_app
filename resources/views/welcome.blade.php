@extends('layouts.app')

@section('content')
    <!-- Start Hero -->
    <section class="bg-half-100 pb-0 bg-soft-secondary d-table w-100 overflow-hidden" style="background: url('/assets/images/shape2.png') top; z-index: 0;">
        <div class="container">
            <div class="row align-items-center mt-5 mt-sm-0">
                <div class="col-md-8">
                    <div class="title-heading text-center text-md-start">
                        <span class="text-uppercase font-weight-bold">All-in-one GST Platform</span>
                        <h4 class="heading mb-3 mt-2">Explore, Find, Ask & Learn.</h4>
                        <hr>
                        <p class="mb-0  mx-auto ms-md-auto">India's most-trusted Cloud Platform For GST Updates,Guides,Tools & Resources in Regional Languages.</p>

                        <div class="mt-4">
                            <form role="search" method="get" _lpchecked="1">
                                <div class="input-group mb-3 border rounded">
                                    <input type="text" id="s" name="s" class="form-control form-control-lg border-0" placeholder="Search Everything you need to know about GST & more.">
                                    <button type="submit" class="input-group-text bg-white border-0" id="searchsubmit"><i class="uil uil-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-md-4 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="freelance-hero position-relative">
                        <div class="bg-shape position-relative">
                            <img src="{{ asset('assets/images/homeLogo@2x.png') }}" class="mx-auto d-block img-fluid" alt="">
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- End Hero -->
    <section class="section bg-light">
        <div class="container">

            <div class="row">
                <div class="col-md-6 mt-4 pt-2">

                        <a href="{{ url('/resources') }}" class="d-flex features block_library feature-clean p-4 bg-white shadow rounded">
                            <div class=" d-block text-center rounded">

                                <img src="{{ asset('assets/images/icon_library.svg') }}">
                            </div>
                            <div class="flex-1 content ms-4">
                                <h5 class="mb-1 text-dark">Explore GST Library</h5>
                                <p class="text-muted mb-0">Access Acts, Notification, Rules, Orders</p>
                            </div>
                        </a>



                </div><!--end col-->

                <div class="col-md-6 mt-4 pt-2">
                    <a href="{{ url('/consult') }}" class="d-flex features block_consult feature-clean  block_primary p-4 bg-white shadow rounded">
                        <div class="icons text-primary d-block  text-center rounded">
                            <img src="{{ asset('assets/images/icon_consult.svg') }}">


                        </div>
                        <div class="flex-1 content ms-4">
                            <h5 class="mb-1 text-dark">Find & Consult a Expert</h5>
                            <p class="text-muted mb-0">Find, select and compare </p>
                        </div>
                    </a>
                </div><!--end col-->

                <div class="col-md-6 mt-4 pt-2">
                    <a  href="{{ url('/community') }}" class="d-flex features feature-clean block_ask p-4 bg-white shadow rounded">
                        <div class="icons text-primary d-block text-center rounded">
                            <img src="{{ asset('assets/images/icon_ask.svg') }}">
                        </div>
                        <div class="flex-1 content ms-4">
                            <h5 class="mb-1 text-dark">Ask a GST Experts</h5>
                            <p class="text-muted mb-0">Get advice from multiple GST experts
                            </p>
                        </div>
                    </a>
                </div><!--end col-->

                <div class="col-md-6 mt-4 pt-2">
                    <a  href="{{ url('/learn') }}"class="d-flex features feature-clean block_learn p-4 bg-white shadow rounded">
                        <div class="icons text-primary d-block text-center rounded">
                            <img src="{{ asset('assets/images/icon_learn.svg') }}">
                        </div>
                        <div class="flex-1 content ms-4">
                            <h5 class="mb-1 text-dark">Learn</h5>
                            <p class="text-muted mb-0">Learn everything about GST for Businesses </p>
                        </div>
                    </a>
                </div><!--end col-->


            </div><!--end row-->
        </div><!--end container-->


    </section>

    <!-- Start -->
    <section class="section  pb-md-6 " id="featured">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 col-lg-4 mb-4 ">
                    <h5 class="font-weight-bold">What's New?</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">

                            <li><a href="#">Advisory for GSTR 9 and GSTR 9C by GSTN</a> </li>
                            <li><a href="#">Representation Seeking Extension Of Due Date For Furnishing GSTR 9C For 2019-20</a></li>
                            <li><a href="#">Applicability of Dynamic QR Code on B2C invoices & compliance.</a></li>
                            <li><a href="#">Advisory on Annual Return (GSTR-9)</a></li>
                            <li><a href="#">Various Important Due Dates released by ICMAI</a></li>


                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>

                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4  border-left">

                    <h5 class="font-weight-bold">Gst Council Meeting Updates</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">
                            <li><a href="#">42nd GST Council Meetings Updates <br> 05th October 2020 </a> </li>
                            <li><a href="#">41st GST Council Meeting  Updates <br> 27th August 2020  </a> </li>
                            <li><a href="#">40th GST Council Meetings Updates <br> 12th June 2020 </a> </li>
                            <li><a href="#">39th GST Council Meetings Updates <br> 05th October 2020 </a> </li>
                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>

                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4  border-left ">
                    <h5 class="font-weight-bold">Notification / Circulars</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">
                            <li><a href="#">Seeks to make amendment (2021) to CGST Rules, 2017</a> </li>
                            <li><a href="#">Commissioner of commercial Taxes Circular No. GST-14/2020-21</a></li>
                            <li><a href="#">Waiver from recording of UIN on the invoices for the months of April 2020 to March 2021.</a></li>
                            <li><a href="#">Seeks to extend the due dates for compliances and actions in respect of anti-profiteering measures under GST till 31.03.2021.</a></li>

                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </section>
    <!-- End -->

    <section class="section">
        <div class="container pb-lg-4 mb-md-5 mb-4 mt-60 mt-60">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title">
                        <h1 class=" mb-4 we_help">Get 14 free days of <br>
                            GstCentral Premium</h1>
                        <p class="text-muted  mx-auto mb-0">No Commitments. Cancel Anytime.</p>

                        <div class="mt-4">
                            <a href="{{ url('/pro') }}" class="btn btn-soft-success"> Subscribe Now! </a>

                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section>

    <section class="section">


        <div class="container mt-4 mt-lg-0">


            <div class="row">
                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="section-title text-center text-md-start">
                        <h4 class="mb-4">Read top articles from GST experts</h4>
                        <p class="text-muted mb-3 para-desc">Gst Related articles that keep you informed about good GST practices and achieve your goals.</p>
                        <a href="#" class="btn btn-soft-success"> See all Articles </a>
                    </div>
                </div><!--end col-->
                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="{{ asset('assets/images/blog/01.jpg') }}" class="card-img-top" alt="...">
                            <div class="overlay rounded-top bg-dark"></div>
                        </div>
                        <div class="card-body content">
                            <h5><a href="javascript:void(0)" class="card-title title text-dark">Common GST Filing Errors You Can

                                 </a></h5>
                            <div class="post-meta d-flex justify-content-between mt-3">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                </ul>
                                <a href="page-blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            </div>
                        </div>
                        <div class="author">
                            <small class="text-light user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                            <small class="text-light date"><i class="uil uil-calendar-alt"></i> 13th August, 2019</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="{{ asset('assets/images/blog/01.jpg') }}" class="card-img-top" alt="...">
                            <div class="overlay rounded-top bg-dark"></div>
                        </div>
                        <div class="card-body content">
                            <h5><a href="javascript:void(0)" class="card-title title text-dark">Due Date Compliance Calendar - March 2021</a></h5>
                            <div class="post-meta d-flex justify-content-between mt-3">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                </ul>
                                <a href="page-blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            </div>
                        </div>
                        <div class="author">
                            <small class="text-light user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                            <small class="text-light date"><i class="uil uil-calendar-alt"></i> 13th August, 2019</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="{{ asset('assets/images/blog/01.jpg') }}" class="card-img-top" alt="...">
                            <div class="overlay rounded-top bg-dark"></div>
                        </div>
                        <div class="card-body content">
                            <h5><a href="javascript:void(0)" class="card-title title text-dark">Smartest Applications for Business</a></h5>
                            <div class="post-meta d-flex justify-content-between mt-3">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                </ul>
                                <a href="page-blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            </div>
                        </div>
                        <div class="author">
                            <small class="text-light user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                            <small class="text-light date"><i class="uil uil-calendar-alt"></i> 13th August, 2019</small>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->


    </section>

    <section class="section  pt-5 mt-4 pb-0">
        <div class="container-fluid mt-md-1 px-0 ">
            <div class="rounded bg-light py-5 px-3 px-sm-0">
                <div class="row">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="title-heading me-lg-4">

                                    <h1 class="heading text-dark mb-3">Start learning  <br> With <span class="text-success">GstCentral</span></h1>
                                    <p class="para-desc text-muted">Get unlimited access to structured courses & doubt clearing sessions.</p>
                                    <a href="#" class="btn btn-soft-success"> Start Learning </a>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2 mt-sm-0 pt-sm-0">
                                <div class="position-relative">
                                    <img src="{{ asset('assets/images/online-learning.svg') }}" class="rounded img-fluid mx-auto d-block" alt="">
                                    <div class="play-icon">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#watchvideomodal" class="play-btn video-play-icon">
                                            <i class="mdi mdi-play text-primary rounded-circle bg-white shadow"></i>
                                        </a>
                                    </div>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end container-->

                    <div class="container mt-100 mt-60">
                        <div class="row justify-content-center">
                            <div class="col-12 text-center">
                                <div class="section-title mb-4 pb-2">
                                    <h4 class="title mb-4">Explore Popular Courses</h4>
                                    <p class="text-muted para-desc mx-auto mb-0">Start working with <span class="text-primary fw-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->

                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                                    <div class="position-relative d-block overflow-hidden">
                                        <img src="{{ asset('assets/images/course/4.jpg') }}" class="img-fluid rounded-top mx-auto" alt="">
                                        <div class="overlay-work bg-dark"></div>
                                        <a href="javascript:void(0)" class="text-white h6 preview">Preview Now <i class="uil uil-angle-right-b align-middle"></i></a>
                                    </div>

                                    <div class="card-body">
                                        <h5><a href="javascript:void(0)" class="title text-dark">
                                                GST Course on Works Contract & Real Estate by CA Aanchal Kapoor</a></h5>
                                        <div class="rating">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item">5 Star (3 <i class="uil uil-user text-muted small"></i>)</li>
                                            </ul>
                                        </div>
                                        <div class="fees d-flex justify-content-between">
                                            <ul class="list-unstyled mb-0 numbers">
                                                <li><i class="uil uil-graduation-cap text-muted"></i> 30 Students</li>
                                                <li><i class="uil uil-notebooks text-muted"></i> 5 Lession</li>
                                            </ul>
                                            <h4><span class="h6">$</span>75</h4>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                                    <div class="position-relative d-block overflow-hidden">
                                        <img src="{{ asset('assets/images/course/4.jpg') }}" class="img-fluid rounded-top mx-auto" alt="">
                                        <div class="overlay-work bg-dark"></div>
                                        <a href="javascript:void(0)" class="text-white h6 preview">Preview Now <i class="uil uil-angle-right-b align-middle"></i></a>
                                    </div>

                                    <div class="card-body">
                                        <h5><a href="javascript:void(0)" class="title text-dark">Access to Higher Education</a></h5>
                                        <div class="rating">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star-outline h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item">3.99 Star (11 <i class="uil uil-user text-muted small"></i>)</li>
                                            </ul>
                                        </div>
                                        <div class="fees d-flex justify-content-between">
                                            <ul class="list-unstyled mb-0 numbers">
                                                <li><i class="uil uil-graduation-cap text-muted"></i> 30 Students</li>
                                                <li><i class="uil uil-notebooks text-muted"></i> 5 Lession</li>
                                            </ul>
                                            <h4><span class="h6">$</span>150</h4>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                                    <div class="position-relative d-block overflow-hidden">
                                        <img src="{{ asset('assets/images/course/4.jpg') }}" class="img-fluid rounded-top mx-auto" alt="">
                                        <div class="overlay-work bg-dark"></div>
                                        <a href="javascript:void(0)" class="text-white h6 preview">Preview Now <i class="uil uil-angle-right-b align-middle"></i></a>
                                    </div>

                                    <div class="card-body">
                                        <h5><a href="javascript:void(0)" class="title text-dark">Course in TEFL Teacher Training</a></h5>
                                        <div class="rating">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star-half h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item">4.7 Star (9 <i class="uil uil-user text-muted small"></i>)</li>
                                            </ul>
                                        </div>
                                        <div class="fees d-flex justify-content-between">
                                            <ul class="list-unstyled mb-0 numbers">
                                                <li><i class="uil uil-graduation-cap text-muted"></i> 30 Students</li>
                                                <li><i class="uil uil-notebooks text-muted"></i> 5 Lession</li>
                                            </ul>
                                            <h4><span class="h6">$</span>175</h4>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                                    <div class="position-relative d-block overflow-hidden">
                                        <img src="{{ asset('assets/images/course/4.jpg') }}" class="img-fluid rounded-top mx-auto" alt="">
                                        <div class="overlay-work bg-dark"></div>
                                        <a href="javascript:void(0)" class="text-white h6 preview">Preview Now <i class="uil uil-angle-right-b align-middle"></i></a>
                                    </div>

                                    <div class="card-body">
                                        <h5><a href="javascript:void(0)" class="title text-dark">Course in TEFL Teacher Training</a></h5>
                                        <div class="rating">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item"><i class="mdi mdi-star-half h5 mb-0 text-warning"></i></li>
                                                <li class="list-inline-item">4.7 Star (9 <i class="uil uil-user text-muted small"></i>)</li>
                                            </ul>
                                        </div>
                                        <div class="fees d-flex justify-content-between">
                                            <ul class="list-unstyled mb-0 numbers">
                                                <li><i class="uil uil-graduation-cap text-muted"></i> 30 Students</li>
                                                <li><i class="uil uil-notebooks text-muted"></i> 5 Lession</li>
                                            </ul>
                                            <h4><span class="h6">$</span>175</h4>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end col-->



                            <div class="col-12 mt-4 pt-4 text-center">
                                <a href="javascript:void(0)" class="btn btn-soft-success">See More Courses <i class="uil uil-angle-right-b align-middle"></i></a>
                            </div>
                        </div><!--end row-->
                    </div>
                </div><!--end row-->
            </div><!--end div-->
        </div><!--end container fluid-->
    </section>



@endsection
