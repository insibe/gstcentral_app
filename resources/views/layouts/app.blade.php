<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.header-scripts')
</head>

<body>
<!-- Loader -->
<!-- <div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
</div> -->
<!-- Loader -->

<!-- Navbar STart -->
<header>
    <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="container">

            <a class=" navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('assets/images/logo.svg') }}" >
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link  active" aria-current="page" href="{{ url('/resources') }}">Resources</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link  " aria-current="page" href="{{ url('/consult') }}">Consult</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="{{ url('/community') }}">Community</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{ url('/learn') }}">Learn</a>
                    </li>




                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @guest
                        <li >
                            <a class="btn btn-outline-success" href="{{ route('login') }}">{{ __('Sign in') }}</a>

                            @if (Route::has('register'))

                                <a class="btn  btn-outline-success" href="{{ route('register') }}">{{ __(' Get Started') }}</a>

                            @endif
                        </li>
                    @else

                        <li class="nav-item dropdown">
                            <a class="btn btn-outline-success" href="{{ url('Create') }}">{{ __('Create') }}</a>
                            <a class="btn  btn-outline-success dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Dashboard
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <em class="icon ni ni-signout"></em><span>Sign out</span></a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>


                    @endguest
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- Navbar End -->

@yield('content')
<!-- End -->

<!-- Footer Start -->
<footer class="footer ">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                <a href="#" class="logo-footer">
                    <img src="{{ asset('assets/images/logo.svg') }}" height="60" alt="">
                </a>
                <p class="mt-4 text-muted">GstCentral™ is the India's most-trusted Cloud Platform For GST Updates, Guides, Tools & Resources in Regional Languages. GstCentral™ is a product by Konni Ventures Pvt.Ltd.</p>
                <ul class="list-unstyled social-icon social mb-0 mt-4">
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="linkedin" class="fea icon-sm fea-social"></i></a></li>
                </ul><!--end icon-->
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-dark footer-head">Company</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{ url('company/about-us') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> About us</a></li>
                    <li><a href="{{ url('company/investor-relations') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Investor Relations</a></li>
                    <li><a href="{{ url('company/careers') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Careers</a></li>
                    <li><a href="{{ url('company/media-kit') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Media Kit</a></li>
                    <li><a href="{{ url('company/contact-us') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Contact Us</a></li>

                </ul>
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-dark footer-head">Solutions</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{ url('solutions/for-finance-professionals') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> For Finance Professionals</a></li>
                    <li><a href="{{ url('solutions/for-finance-aspirants') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> For Finance Aspirants</a></li>
                    <li><a href="{{ url('solutions/for-businesses') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> For Businesses</a></li>

                </ul>
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-dark footer-head">Quick Links</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{ url('/pricing') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Pricing</a></li>
                    <li><a href="{{ url('/refund-policy') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Refund Policy</a></li>
                    <li><a href="{{ url('/payment-terms') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Payment Terms</a></li>


                </ul>
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-dark footer-head">Resources</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{ url('/support-center') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Support Center</a></li>
                    <li><a href="{{ url('/product-roadmap') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Product Roadmap</a></li>
                    <li><a href="{{ url('/changelog') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Changelog</a></li>
                    <li><a href="{{ url('/developers') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Developers</a></li>
                </ul>
            </div><!--end col-->
        </div><!--end row-->
        <div class="row ">
            <div class="col-sm-12">
                <div class="copyright-bottom">
                    <h6 class="text-muted">About GstCentral™ - All-in-one GST App</h6>
                    <p class="text-muted">GSTCentral™ is the All-in-one GST App, that has packages of features and tools to leverage the existing Financial Service providers and Entrepreneurs with free latest information and premium services related to GST.The ‘GSTCentral’ App is an innovative attempt to simplify and redesign the way GST is perceived. Equipped with the latest user-friendly interfaces and AI technology, providing personalized information as per the user requirements.</p>
                    <h6 class="text-muted">GST - Goods and Service Tax in India - What is GST?</h6>
                    <p class="text-muted">GST is known as the Goods and Services Tax. It is an indirect tax which has replaced many indirect taxes in India such as the excise duty, VAT, services tax, etc. The Goods and Service Tax Act was passed in the Parliament on 29th March 2017 and came into effect on 1st July 2017.</p>
                    <hr>
                    <p>By continuing past this page, you agree to our <a href="#">Terms of Service</a>,<a href="#">Cookie Policy</a>,<a href="#">Privacy Policy</a> and <a href="#">Content Policy</a>. © 2021 - <a href="#">Konni Ventures Pvt Ltd</a> . All rights reserved.</p>
                </div>
            </div><!--end col-->

        </div><!--end row-->
    </div><!--end container-->

</footer><!--end footer-->

<!-- Footer End -->

<!-- Back to top -->
<a href="#" onclick="topFunction()" id="back-to-top" class="btn btn-icon btn-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
<!-- Back to top -->


<!-- javascript -->
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<!-- SLIDER -->
<script src="{{ asset('assets/js/tiny-slider.js') }} "></script>
<script src="{{ asset('assets/js/tiny-slider-init.js') }} "></script>
<!-- Icons -->
<script src="{{ asset('assets/js/feather.min.js') }}"></script>
<!-- Main Js -->
<script src="{{ asset('assets/js/app.js') }}"></script>
</body>
</html>
