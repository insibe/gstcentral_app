<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>GstCentral | All-in-one GST Cloud Platform</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="India's most-trusted Cloud Platform For GST Updates, Guides,Tools & Resources in Regional Languages." />
    <meta name="keywords" content="Saas, Software, multi-uses, HTML, Clean, Modern" />
    <meta name="author" content="GstCentral" />
    <meta name="email" content="info@gstcentral.com" />
    <meta name="website" content="gstcentral.com" />
    <meta name="Version" content="v0.1" />
    <!-- favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <!-- Bootstrap -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="{{ asset('assets/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.6/css/line.css">
    <!-- Slider -->
    <link rel="stylesheet" href="{{ asset('assets/css/tiny-slider.css') }}"/>
    <!-- Main Css -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="{{ asset('assets/css/colors/default.css')}}" rel="stylesheet" id="color-opt">

</head>

<body>
<!-- Loader -->
<!-- <div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
</div> -->
<!-- Loader -->

<!-- Navbar STart -->
<header id="topnav" class="defaultscroll sticky">
    <div class="container">
        <!-- Logo container-->
        <div>
            <a class="logo" href="{{ url('/') }}">
                <img src="{{ asset('assets/images/logo.svg') }}" >
            </a>
        </div>
        <div class="buy-button">
            <a href="{{ url('/login') }}"  class="btn btn-outline-light">Log in</a>
            <a href="{{ url('/register') }}"  class="btn btn btn-outline-light">Get Started</a>
        </div><!--end login button-->
        <!-- End Logo container-->
        <div class="menu-extras">
            <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle" id="isToggle" onclick="toggleMenu()">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </div>
        </div>

        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu nav-light">
                <li><a href="{{ url('/resources') }}" class="sub-menu-item">Resources</a></li>
                <li><a href="{{ url('/consult') }}" class="sub-menu-item">Consult</a></li>
                <li><a href="{{ url('/community') }}" class="sub-menu-item">Community</a></li>
                <li><a href="{{ url('/learn') }}" class="sub-menu-item">learn</a></li>


            </ul><!--end navigation menu-->
            <div class="buy-menu-btn d-none">
                <a href="{{ url('/login') }}" target="_blank" class="btn btn-primary">Log in</a>
                <a href="{{ url('/register') }}" target="_blank" class="btn btn-primary">Get Started</a>
            </div><!--end login button-->
        </div><!--end navigation-->
    </div><!--end container-->
</header><!--end header-->
<!-- Navbar End -->
<section class="bg-half bg-dark d-table w-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title"> About us </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="index.html">Landrick</a></li>
                                <li class="breadcrumb-item"><a href="#">Page</a></li>
                                <li class="breadcrumb-item active" aria-current="page">About Us</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section>
@yield('content')
<!-- End -->

<!-- Footer Start -->
<footer class="footer bg-theme">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                <a href="#" class="logo-footer">
                    <img src="images/logo-dark.png" height="24" alt="">
                </a>
                <p class="mt-4 text-muted">GstCentral™ is the India's most-trusted Cloud Platform For GST Updates, Guides, Tools & Resources in Regional Languages. GstCentral™ is a product by Konni Ventures Pvt.Ltd.</p>
                <ul class="list-unstyled social-icon social mb-0 mt-4">
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="linkedin" class="fea icon-sm fea-social"></i></a></li>
                </ul><!--end icon-->
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-dark footer-head">Company</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{ url('company/about-us') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> About us</a></li>
                    <li><a href="{{ url('company/investor-relations') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Investor Relations</a></li>
                    <li><a href="{{ url('company/careers') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Careers</a></li>
                    <li><a href="{{ url('company/media-kit') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Media Kit</a></li>
                    <li><a href="{{ url('company/contact-us') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Contact Us</a></li>

                </ul>
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-dark footer-head">Solutions</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{ url('solutions/for-finance-professionals') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> For Finance Professionals</a></li>
                    <li><a href="{{ url('solutions/for-finance-aspirants') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> For Finance Aspirants</a></li>
                    <li><a href="{{ url('solutions/for-businesses') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> For Businesses</a></li>

                </ul>
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-dark footer-head">Quick Links</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{ url('/pricing') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Pricing</a></li>
                    <li><a href="{{ url('/refund-policy') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Refund Policy</a></li>
                    <li><a href="{{ url('/payment-terms') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Payment Terms</a></li>


                </ul>
            </div><!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h5 class="text-dark footer-head">Resources</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{ url('/support-center') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Support Center</a></li>
                    <li><a href="{{ url('/product-roadmap') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Product Roadmap</a></li>
                    <li><a href="{{ url('/changelog') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Changelog</a></li>
                    <li><a href="{{ url('/developers') }}" class="text-muted"><i class="uil uil-angle-right-b me-1"></i> Developers</a></li>
                </ul>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</footer><!--end footer-->
<footer class="footer footer-bar">
    <div class="container ">
        <div class="row ">
            <div class="col-sm-12">
                <div class="copyright-bottom">
                    <h6 class="text-muted">About GstCentral™ - All-in-one GST App</h6>
                    <p class="text-muted">GSTCentral™ is the All-in-one GST App, that has packages of features and tools to leverage the existing Financial Service providers and Entrepreneurs with free latest information and premium services related to GST.The ‘GSTCentral’ App is an innovative attempt to simplify and redesign the way GST is perceived. Equipped with the latest user-friendly interfaces and AI technology, providing personalized information as per the user requirements.</p>
                    <h6 class="text-muted">GST - Goods and Service Tax in India - What is GST?</h6>
                    <p class="text-muted">GST is known as the Goods and Services Tax. It is an indirect tax which has replaced many indirect taxes in India such as the excise duty, VAT, services tax, etc. The Goods and Service Tax Act was passed in the Parliament on 29th March 2017 and came into effect on 1st July 2017.</p>
                    <hr>
                    <p>By continuing past this page, you agree to our <a href="#">Terms of Service</a>,<a href="#">Cookie Policy</a>,<a href="#">Privacy Policy</a> and <a href="#">Content Policy</a>. © 2021 - <a href="#">Konni Ventures Pvt Ltd</a> . All rights reserved.</p>
                </div>
            </div><!--end col-->

            <div class="col-sm-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <ul class="list-unstyled text-sm-end mb-0">
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="images/payments/american-ex.png" class="avatar avatar-ex-sm" title="American Express" alt=""></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="images/payments/discover.png" class="avatar avatar-ex-sm" title="Discover" alt=""></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="images/payments/master-card.png" class="avatar avatar-ex-sm" title="Master Card" alt=""></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="images/payments/paypal.png" class="avatar avatar-ex-sm" title="Paypal" alt=""></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="images/payments/visa.png" class="avatar avatar-ex-sm" title="Visa" alt=""></a></li>
                </ul>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</footer><!--end footer-->
<!-- Footer End -->

<!-- Back to top -->
<a href="#" onclick="topFunction()" id="back-to-top" class="btn btn-icon btn-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
<!-- Back to top -->


<!-- javascript -->
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<!-- SLIDER -->
<script src="{{ asset('assets/js/tiny-slider.js') }} "></script>
<script src="{{ asset('assets/js/tiny-slider-init.js') }} "></script>
<!-- Icons -->
<script src="{{ asset('assets/js/feather.min.js') }}"></script>
<!-- Main Js -->
<script src="{{ asset('assets/js/app.js') }}"></script>
</body>
</html>
