@extends('layouts.inner')

@section('content')

    <section>
        <div class="container-fluid mt-md-1 px-0 px-md-3">
            <div class="rounded bg-light py-5 px-3 px-sm-0">
                <div class="row">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-7 col-md-6 col-12">
                                <div class="title-heading me-lg-4">
                                    <div class="alert alert-primary alert-pills shadow" role="alert">
                                        <span class="content"> Good bye to GST Problems</span>
                                    </div>

                                    <h1 class="heading text-dark mb-3">Share your queries <br> related to <span class="text-success">GST  </span></h1>
                                    <p class="para-desc text-muted"> Get answers within 24 hours</p>
                                    <div class="subcribe-form mt-4 pt-2">
                                        <a href="#" class="btn btn-soft-success">Get started </a>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-5 col-md-6 col-12 mt-4 pt-2 mt-sm-0 pt-sm-0">
                                <div class="position-relative">
                                    <img src="{{ asset('assets/images/community.svg') }}" class="rounded img-fluid mx-auto d-block" alt="">

                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end container-->
                </div><!--end row-->
            </div><!--end div-->
        </div><!--end container fluid-->
    </section><!--end section-->
    <section class="section">
        <div class="container">

        </div><!--end container-->
    </section>
@endsection
