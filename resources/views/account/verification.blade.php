

@extends('layouts.inner')

@section('content')

    <section class="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="{{ asset('assets/images/verifier.png') }}" class="me-md-4" alt="">
                </div><!--end col-->

                <div class="col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <div class="section-title ms-lg-5">
                        <span class="badge rounded-pill bg-soft-primary">VERIFY YOUR ACCOUNT</span>
                        <h1 class="heading text-dark mb-3">It only takes <br><span class="text-success">a few seconds</span> </h1>
                        <p class="text-muted">In order to verify user authenticity and prevent spam we just need a quick phone verification before you can start submitting content to the Gst Central.</p>
                        <a href="javascript:void(0)" class="btn  btn-lg btn-primary"> Get Verified <i class="uil uil-angle-right-b"></i></a>
                        <a href="javascript:void(0)" class="btn btn-lg btn-soft-secondary"> No Thanks <i class="uil uil-angle-right-b"></i></a>
                        <p class=" text-sm text-muted mt-4 m-0">We respect your privacy and will never send you messages.</p>
                        <p class="text-sm text-muted m-0">We do not store your number, we only use it to verify your account.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
        >
    </section>

@endsection
