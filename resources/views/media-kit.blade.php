@extends('layouts.inner')

@section('content')
    <section class=" section bg-soft-success">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form role="search" method="get" _lpchecked="1">
                        <div class="input-group mb-3 border rounded">
                            <input type="text" id="s" name="s" class="form-control form-control-lg border-0" placeholder="Search Everything you need to know about GST & more.">
                            <button type="submit" class="input-group-text bg-white border-0" id="searchsubmit"><i class="uil uil-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <div class="row align-items-center" id="counter">
                <div class="col-md-6">
                    <img src="images/company/about2.png" class="img-fluid" alt="">
                </div><!--end col-->

                <div class="col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="ms-lg-4">
                        <div class="d-flex mb-4">
                            <span class="text-primary h1 mb-0"><span class="counter-value display-1 fw-bold" data-target="15">15</span>+</span>
                            <span class="h6 align-self-end ms-2">Years <br> Experience</span>
                        </div>
                        <div class="section-title">
                            <h4 class="title mb-4">Who we are ?</h4>
                            <p class="text-muted">Start working with <span class="text-primary fw-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect. Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with 'real' content. This is required when, for example, the final text is not yet available. Dummy texts have been in use by typesetters since the 16th century.</p>
                            <a href="javascript:void(0)" class="btn btn-primary mt-3">Contact us</a>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

        <div class="container mt-4">
            <div class="row justify-content-center">
                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/amazon.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/google.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/lenovo.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/paypal.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/shopify.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/spotify.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section>


@endsection
