@extends('layouts.inner')

@section('content')
    <section>
        <div class="container-fluid mt-md-1 px-0 px-md-3">
            <div class="rounded bg-light py-5 px-3 px-sm-0">
                <div class="row">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-7 col-md-6 col-12">
                                <div class="title-heading me-lg-4">
                                    <div class="alert alert-primary alert-pills shadow" role="alert">
                                        <span class="content"> Skip the travel!</span>
                                    </div>

                                    <h1 class="heading text-dark mb-3">Take Online Consultation <br> With <span class="text-success"> GST Experts</span></h1>
                                    <p class="para-desc text-muted">30 Min Private consultation + Audio call · Starts at just ₹499</p>
                                    <div class="subcribe-form mt-4 pt-2">
                                        <a href="#" class="btn btn-soft-success">Get started </a>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-5 col-md-6 col-12 mt-4 pt-2 mt-sm-0 pt-sm-0">
                                <div class="position-relative">
                                    <img src="{{ asset('assets/images/online-consultation.svg') }}" class="rounded img-fluid mx-auto d-block" alt="">

                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end container-->
                </div><!--end row-->
            </div><!--end div-->
        </div><!--end container fluid-->
    </section><!--end section-->

    <section class="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-9 col-md-8">
                    <div class="section-title">
                        <h6 class="mb-0">Showing 1 – 8 of 10 results</h6>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-4 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <div class="form custom-form">
                        <div class="mb-0">
                            <select class="form-select form-control" aria-label="Default select example" id="Sortbylist-job">
                                <option selected="">Default</option>
                                <option>Newest</option>
                                <option>Oldest</option>
                                <option>Random</option>
                            </select>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="candidate-list card rounded border-0 shadow">
                        <div class="card-body">
                            <ul class="list-unstyled align-items-center">
                                <li class="list-inline-item float-end"><a href="javascript:void(0)" class="text-muted like"><i class="mdi mdi-heart h5 mb-0"></i></a></li>
                                <li class="list-inline-item"><span class="badge rounded-pill bg-soft-success">Featured</span></li>
                            </ul>

                            <div class="content text-center">
                                <img src="images/client/01.jpg" class="avatar avatar-md-md shadow-md rounded-circle" alt="">
                                <ul class="list-unstyled mb-1 mt-2">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <a href="page-job-candidate.html" class="text-dark h5 name">Calvin Carlo</a>
                                <p class="text-muted my-1">Front-end Developer</p>

                                <span class="text-muted"><i class="uil uil-graduation-cap h4 mb-0 me-2 text-primary"></i>Experience <span class="text-success">3+ years</span></span>

                                <ul class="list-unstyled mt-3">
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">PHP</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">WordPress</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">Web Design</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">CSS</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">JS</a></li>
                                </ul>
                                <div class="d-grid">
                                    <a href="page-job-candidate.html" class="btn btn-soft-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="candidate-list card rounded border-0 shadow">
                        <div class="card-body">
                            <ul class="list-unstyled align-items-center">
                                <li class="list-inline-item float-end"><a href="javascript:void(0)" class="text-muted like"><i class="mdi mdi-heart h5 mb-0"></i></a></li>
                                <li class="list-inline-item"><span class="badge rounded-pill bg-soft-success">Featured</span></li>
                            </ul>

                            <div class="content text-center">
                                <img src="images/client/02.jpg" class="avatar avatar-md-md shadow-md rounded-circle" alt="">
                                <ul class="list-unstyled mb-1 mt-2">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <a href="page-job-candidate.html" class="text-dark h5 name">Martha Griffin</a>
                                <p class="text-muted my-1">WordPress Developer</p>

                                <span class="text-muted"><i class="uil uil-graduation-cap h4 mb-0 me-2 text-primary"></i>Experience <span class="text-success">3+ years</span></span>

                                <ul class="list-unstyled mt-3">
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">PHP</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">WordPress</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">Web Design</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">CSS</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">JS</a></li>
                                </ul>
                                <div class="d-grid">
                                    <a href="page-job-candidate.html" class="btn btn-soft-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="candidate-list card rounded border-0 shadow">
                        <div class="card-body">
                            <ul class="list-unstyled align-items-center">
                                <li class="list-inline-item float-end"><a href="javascript:void(0)" class="text-muted like"><i class="mdi mdi-heart h5 mb-0"></i></a></li>
                                <li class="list-inline-item"><span class="badge rounded-pill bg-soft-success">Featured</span></li>
                            </ul>

                            <div class="content text-center">
                                <img src="images/client/03.jpg" class="avatar avatar-md-md shadow-md rounded-circle" alt="">
                                <ul class="list-unstyled mb-1 mt-2">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <a href="page-job-candidate.html" class="text-dark h5 name">Ashley Jen</a>
                                <p class="text-muted my-1">Back-end Developer</p>

                                <span class="text-muted"><i class="uil uil-graduation-cap h4 mb-0 me-2 text-primary"></i>Experience <span class="text-success">3+ years</span></span>

                                <ul class="list-unstyled mt-3">
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">PHP</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">WordPress</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">Web Design</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">CSS</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">JS</a></li>
                                </ul>
                                <div class="d-grid">
                                    <a href="page-job-candidate.html" class="btn btn-soft-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="candidate-list card rounded border-0 shadow">
                        <div class="card-body">
                            <ul class="list-unstyled align-items-center">
                                <li class="list-inline-item float-end"><a href="javascript:void(0)" class="text-muted like"><i class="mdi mdi-heart h5 mb-0"></i></a></li>
                                <li class="list-inline-item"><span class="badge rounded-pill bg-soft-success">Featured</span></li>
                            </ul>

                            <div class="content text-center">
                                <img src="images/client/04.jpg" class="avatar avatar-md-md shadow-md rounded-circle" alt="">
                                <ul class="list-unstyled mb-1 mt-2">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <a href="page-job-candidate.html" class="text-dark h5 name">Nicole Alan</a>
                                <p class="text-muted my-1">UX Designer</p>

                                <span class="text-muted"><i class="uil uil-graduation-cap h4 mb-0 me-2 text-primary"></i>Experience <span class="text-success">3+ years</span></span>

                                <ul class="list-unstyled mt-3">
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">PHP</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">WordPress</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">Web Design</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">CSS</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">JS</a></li>
                                </ul>
                                <div class="d-grid">
                                    <a href="page-job-candidate.html" class="btn btn-soft-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="candidate-list card rounded border-0 shadow">
                        <div class="card-body">
                            <ul class="list-unstyled align-items-center">
                                <li class="list-inline-item float-end"><a href="javascript:void(0)" class="text-muted like"><i class="mdi mdi-heart h5 mb-0"></i></a></li>
                                <li class="list-inline-item"><span class="badge rounded-pill bg-soft-success">Featured</span></li>
                            </ul>

                            <div class="content text-center">
                                <img src="images/client/05.jpg" class="avatar avatar-md-md shadow-md rounded-circle" alt="">
                                <ul class="list-unstyled mb-1 mt-2">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <a href="page-job-candidate.html" class="text-dark h5 name">Jennifer Pham</a>
                                <p class="text-muted my-1">Web Designer</p>

                                <span class="text-muted"><i class="uil uil-graduation-cap h4 mb-0 me-2 text-primary"></i>Experience <span class="text-success">3+ years</span></span>

                                <ul class="list-unstyled mt-3">
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">PHP</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">WordPress</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">Web Design</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">CSS</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">JS</a></li>
                                </ul>
                                <div class="d-grid">
                                    <a href="page-job-candidate.html" class="btn btn-soft-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="candidate-list card rounded border-0 shadow">
                        <div class="card-body">
                            <ul class="list-unstyled align-items-center">
                                <li class="list-inline-item float-end"><a href="javascript:void(0)" class="text-muted like"><i class="mdi mdi-heart h5 mb-0"></i></a></li>
                                <li class="list-inline-item"><span class="badge rounded-pill bg-soft-success">Featured</span></li>
                            </ul>

                            <div class="content text-center">
                                <img src="images/client/06.jpg" class="avatar avatar-md-md shadow-md rounded-circle" alt="">
                                <ul class="list-unstyled mb-1 mt-2">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <a href="page-job-candidate.html" class="text-dark h5 name">Alex Tom</a>
                                <p class="text-muted my-1">UI Designer</p>

                                <span class="text-muted"><i class="uil uil-graduation-cap h4 mb-0 me-2 text-primary"></i>Experience <span class="text-success">3+ years</span></span>

                                <ul class="list-unstyled mt-3">
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">PHP</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">WordPress</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">Web Design</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">CSS</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">JS</a></li>
                                </ul>
                                <div class="d-grid">
                                    <a href="page-job-candidate.html" class="btn btn-soft-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="candidate-list card rounded border-0 shadow">
                        <div class="card-body">
                            <ul class="list-unstyled align-items-center">
                                <li class="list-inline-item float-end"><a href="javascript:void(0)" class="text-muted like"><i class="mdi mdi-heart h5 mb-0"></i></a></li>
                                <li class="list-inline-item"><span class="badge rounded-pill bg-soft-success">Featured</span></li>
                            </ul>

                            <div class="content text-center">
                                <img src="images/client/07.jpg" class="avatar avatar-md-md shadow-md rounded-circle" alt="">
                                <ul class="list-unstyled mb-1 mt-2">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <a href="page-job-candidate.html" class="text-dark h5 name">Cristino Murphy</a>
                                <p class="text-muted my-1">PHP Developer</p>

                                <span class="text-muted"><i class="uil uil-graduation-cap h4 mb-0 me-2 text-primary"></i>Experience <span class="text-success">3+ years</span></span>

                                <ul class="list-unstyled mt-3">
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">PHP</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">WordPress</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">Web Design</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">CSS</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">JS</a></li>
                                </ul>
                                <div class="d-grid">
                                    <a href="page-job-candidate.html" class="btn btn-soft-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="candidate-list card rounded border-0 shadow">
                        <div class="card-body">
                            <ul class="list-unstyled align-items-center">
                                <li class="list-inline-item float-end"><a href="javascript:void(0)" class="text-muted like"><i class="mdi mdi-heart h5 mb-0"></i></a></li>
                                <li class="list-inline-item"><span class="badge rounded-pill bg-soft-success">Featured</span></li>
                            </ul>

                            <div class="content text-center">
                                <img src="images/client/08.jpg" class="avatar avatar-md-md shadow-md rounded-circle" alt="">
                                <ul class="list-unstyled mb-1 mt-2">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <a href="page-job-candidate.html" class="text-dark h5 name">Arlo Sons</a>
                                <p class="text-muted my-1">React Developer</p>

                                <span class="text-muted"><i class="uil uil-graduation-cap h4 mb-0 me-2 text-primary"></i>Experience <span class="text-success">3+ years</span></span>

                                <ul class="list-unstyled mt-3">
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">PHP</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">WordPress</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">Web Design</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">CSS</a></li>
                                    <li class="list-inline-item m-1"><a href="jvascript:void(0)" class="rounded bg-light py-1 px-2 text-muted small">JS</a></li>
                                </ul>
                                <div class="d-grid">
                                    <a href="page-job-candidate.html" class="btn btn-soft-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <!-- PAGINATION START -->
                <div class="col-12 mt-4 pt-2">
                    <ul class="pagination justify-content-center mb-0">
                        <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Previous">Prev</a></li>
                        <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Next">Next</a></li>
                    </ul>
                </div><!--end col-->
                <!-- PAGINATION END -->
            </div><!--end row-->
        </div><!--end container-->
    </section>
@endsection
