@extends('layouts.inner')

@section('content')
    <!-- Hero Start -->
    <section>
        <div class="container-fluid mt-md-1 px-0 px-md-3">
            <div class="rounded bg-light py-5 px-3 px-sm-0">
                <div class="row">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-7 col-md-6 col-12">
                                <div class="title-heading me-lg-4">
                                    <div class="alert alert-primary alert-pills shadow" role="alert">
                                        <span class="content"> Are you ready to learn online ?</span>
                                    </div>

                                    <h1 class="heading text-dark mb-3">Start learning  With  <br> With <span class="text-success">Top Educators</span></h1>
                                    <p class="para-desc text-muted">Get access to structured courses & doubt clearing sessions.</p>
                                    <div class="subcribe-form mt-4 pt-2">
                                        <a href="#" class="btn btn-soft-success">Get started </a>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-5 col-md-6 col-12 mt-4 pt-2 mt-sm-0 pt-sm-0">
                                <div class="position-relative">
                                    <img src="{{ asset('assets/images/online-learning.svg') }}" class="rounded img-fluid mx-auto d-block" alt="">

                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end container-->
                </div><!--end row-->
            </div><!--end div-->
        </div><!--end container fluid-->
    </section><!--end section-->
    <!-- Hero End -->


    <!-- Start -->
    <section>
        <!-- Popular Course Start -->
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="section-title text-center mb-4 pb-2">
                        <h4 class="title mb-4">Popular Courses</h4>
                        <p class="text-muted para-desc mb-0 mx-auto">Start working with <span class="text-primary fw-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="images/course/1.jpg" class="card-img-top" alt="...">
                            <div class="overlay bg-dark"></div>
                            <div class="teacher d-flex align-items-center">
                                <img src="images/client/01.jpg" class="avatar avatar-md-sm rounded-circle shadow" alt="">
                                <div class="ms-2">
                                    <h6 class="mb-0"><a href="javascript:void(0)" class="text-light user">Dung Lewis</a></h6>
                                    <p class="text-light small my-0">Professor</p>
                                </div>
                            </div>
                            <div class="course-fee bg-white text-center shadow-lg rounded-circle">
                                <h6 class="text-primary fw-bold fee">$11</h6>
                            </div>
                        </div>
                        <div class="position-relative">
                            <div class="shape overflow-hidden text-white">
                                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="card-body content">
                            <small><a href="javascript:void(0)" class="text-primary h6">Design</a></small>
                            <h5 class="mt-2"><a href="javascript:void(0)" class="title text-dark">GST Course on Works Contract & Real Estate </a></h5>
                            <p class="text-muted">The most well-known dummy text is the have originated in the 16th century.</p>
                            <a href="javascript:void(0)" class="text-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            <ul class="list-unstyled d-flex justify-content-between border-top mt-3 pt-3 mb-0">
                                <li class="text-muted small"><i class="uil uil-book-open text-info"></i> 25 Lectures</li>
                                <li class="text-muted small ms-3"><i class="uil uil-clock text-warning"></i> 1h 30m</li>
                                <li class="text-muted small ms-3"><i class="uil uil-eye text-primary"></i> 3012</li>
                            </ul>
                        </div>
                    </div> <!--end card / course-blog-->
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="images/course/2.jpg" class="card-img-top" alt="...">
                            <div class="overlay bg-dark"></div>
                            <div class="teacher d-flex align-items-center">
                                <img src="images/client/02.jpg" class="avatar avatar-md-sm rounded-circle shadow" alt="">
                                <div class="ms-2">
                                    <h6 class="mb-0"><a href="javascript:void(0)" class="text-light user">Lisa Marvel</a></h6>
                                    <p class="text-light small my-0">Professor</p>
                                </div>
                            </div>
                            <div class="course-fee bg-white text-center shadow-lg rounded-circle">
                                <h6 class="text-primary fw-bold fee">$15</h6>
                            </div>
                        </div>
                        <div class="position-relative">
                            <div class="shape overflow-hidden text-white">
                                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="card-body content">
                            <small><a href="javascript:void(0)" class="text-primary h6">Development</a></small>
                            <h5 class="mt-2"><a href="javascript:void(0)" class="title text-dark">Online Course on GST Rulings & Judgements</a></h5>
                            <p class="text-muted">The most well-known dummy text is the have originated in the 16th century.</p>
                            <a href="javascript:void(0)" class="text-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            <ul class="list-unstyled d-flex justify-content-between border-top mt-3 pt-3 mb-0">
                                <li class="text-muted small"><i class="uil uil-book-open text-info"></i> 25 Lectures</li>
                                <li class="text-muted small ms-3"><i class="uil uil-clock text-warning"></i> 1h 30m</li>
                                <li class="text-muted small ms-3"><i class="uil uil-eye text-primary"></i> 3012</li>
                            </ul>
                        </div>
                    </div> <!--end card / course-blog-->
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="images/course/3.jpg" class="card-img-top" alt="...">
                            <div class="overlay bg-dark"></div>
                            <div class="teacher d-flex align-items-center">
                                <img src="images/client/03.jpg" class="avatar avatar-md-sm rounded-circle shadow" alt="">
                                <div class="ms-2">
                                    <h6 class="mb-0"><a href="javascript:void(0)" class="text-light user">Amanda Carlo</a></h6>
                                    <p class="text-light small my-0">Professor</p>
                                </div>
                            </div>
                            <div class="course-fee bg-white text-center shadow-lg rounded-circle">
                                <h6 class="text-primary fw-bold fee">$19</h6>
                            </div>
                        </div>
                        <div class="position-relative">
                            <div class="shape overflow-hidden text-white">
                                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="card-body content">
                            <small><a href="javascript:void(0)" class="text-primary h6">Software</a></small>
                            <h5 class="mt-2"><a href="javascript:void(0)" class="title text-dark">Educational Communication</a></h5>
                            <p class="text-muted">The most well-known dummy text is the have originated in the 16th century.</p>
                            <a href="javascript:void(0)" class="text-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            <ul class="list-unstyled d-flex justify-content-between border-top mt-3 pt-3 mb-0">
                                <li class="text-muted small"><i class="uil uil-book-open text-info"></i> 25 Lectures</li>
                                <li class="text-muted small ms-3"><i class="uil uil-clock text-warning"></i> 1h 30m</li>
                                <li class="text-muted small ms-3"><i class="uil uil-eye text-primary"></i> 3012</li>
                            </ul>
                        </div>
                    </div> <!--end card / course-blog-->
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="images/course/4.jpg" class="card-img-top" alt="...">
                            <div class="overlay bg-dark"></div>
                            <div class="teacher d-flex align-items-center">
                                <img src="images/client/04.jpg" class="avatar avatar-md-sm rounded-circle shadow" alt="">
                                <div class="ms-2">
                                    <h6 class="mb-0"><a href="javascript:void(0)" class="text-light user">Anne McKnight</a></h6>
                                    <p class="text-light small my-0">Professor</p>
                                </div>
                            </div>
                            <div class="course-fee bg-white text-center shadow-lg rounded-circle">
                                <h6 class="text-primary fw-bold fee">$9</h6>
                            </div>
                        </div>
                        <div class="position-relative">
                            <div class="shape overflow-hidden text-white">
                                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="card-body content">
                            <small><a href="javascript:void(0)" class="text-primary h6">Music</a></small>
                            <h5 class="mt-2"><a href="javascript:void(0)" class="title text-dark">Introduction to Epidemiology</a></h5>
                            <p class="text-muted">The most well-known dummy text is the have originated in the 16th century.</p>
                            <a href="javascript:void(0)" class="text-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            <ul class="list-unstyled d-flex justify-content-between border-top mt-3 pt-3 mb-0">
                                <li class="text-muted small"><i class="uil uil-book-open text-info"></i> 25 Lectures</li>
                                <li class="text-muted small ms-3"><i class="uil uil-clock text-warning"></i> 1h 30m</li>
                                <li class="text-muted small ms-3"><i class="uil uil-eye text-primary"></i> 3012</li>
                            </ul>
                        </div>
                    </div> <!--end card / course-blog-->
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="images/course/5.jpg" class="card-img-top" alt="...">
                            <div class="overlay bg-dark"></div>
                            <div class="teacher d-flex align-items-center">
                                <img src="images/client/05.jpg" class="avatar avatar-md-sm rounded-circle shadow" alt="">
                                <div class="ms-2">
                                    <h6 class="mb-0"><a href="javascript:void(0)" class="text-light user">Leosy Murfhy</a></h6>
                                    <p class="text-light small my-0">Professor</p>
                                </div>
                            </div>
                            <div class="course-fee bg-white text-center shadow-lg rounded-circle">
                                <h6 class="text-primary fw-bold fee">$24</h6>
                            </div>
                        </div>
                        <div class="position-relative">
                            <div class="shape overflow-hidden text-white">
                                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="card-body content">
                            <small><a href="javascript:void(0)" class="text-primary h6">Art & Fashion</a></small>
                            <h5 class="mt-2"><a href="javascript:void(0)" class="title text-dark">Good Clinical Practice</a></h5>
                            <p class="text-muted">The most well-known dummy text is the have originated in the 16th century.</p>
                            <a href="javascript:void(0)" class="text-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            <ul class="list-unstyled d-flex justify-content-between border-top mt-3 pt-3 mb-0">
                                <li class="text-muted small"><i class="uil uil-book-open text-info"></i> 25 Lectures</li>
                                <li class="text-muted small ms-3"><i class="uil uil-clock text-warning"></i> 1h 30m</li>
                                <li class="text-muted small ms-3"><i class="uil uil-eye text-primary"></i> 3012</li>
                            </ul>
                        </div>
                    </div> <!--end card / course-blog-->
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card blog rounded border-0 shadow overflow-hidden">
                        <div class="position-relative">
                            <img src="images/course/6.jpg" class="card-img-top" alt="...">
                            <div class="overlay bg-dark"></div>
                            <div class="teacher d-flex align-items-center">
                                <img src="images/client/06.jpg" class="avatar avatar-md-sm rounded-circle shadow" alt="">
                                <div class="ms-2">
                                    <h6 class="mb-0"><a href="javascript:void(0)" class="text-light user">Cristino Murfhy</a></h6>
                                    <p class="text-light small my-0">Professor</p>
                                </div>
                            </div>
                            <div class="course-fee bg-white text-center shadow-lg rounded-circle">
                                <h6 class="text-primary fw-bold fee">$21</h6>
                            </div>
                        </div>
                        <div class="position-relative">
                            <div class="shape overflow-hidden text-white">
                                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="card-body content">
                            <small><a href="javascript:void(0)" class="text-primary h6">Programmer</a></small>
                            <h5 class="mt-2"><a href="javascript:void(0)" class="title text-dark">Social Computing</a></h5>
                            <p class="text-muted">The most well-known dummy text is the have originated in the 16th century.</p>
                            <a href="javascript:void(0)" class="text-primary">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                            <ul class="list-unstyled d-flex justify-content-between border-top mt-3 pt-3 mb-0">
                                <li class="text-muted small"><i class="uil uil-book-open text-info"></i> 25 Lectures</li>
                                <li class="text-muted small ms-3"><i class="uil uil-clock text-warning"></i> 1h 30m</li>
                                <li class="text-muted small ms-3"><i class="uil uil-eye text-primary"></i> 3012</li>
                            </ul>
                        </div>
                    </div> <!--end card / course-blog-->
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
        <!-- Popular Course End -->



        <!-- Teachers Start -->
        <div class="container mt-100 mt-60">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="section-title text-center mb-4 pb-2">
                        <h4 class="title mb-4">Our Instructor</h4>
                        <p class="text-muted para-desc mb-0 mx-auto">Start working with <span class="text-primary fw-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="d-flex align-items-center">
                        <img src="images/client/05.jpg" class="avatar avatar-medium rounded-circle img-thumbnail" alt="">
                        <div class="flex-1 content ms-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark">Krista John</a></h5>
                            <small class="position text-muted">Professor</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="d-flex align-items-center">
                        <img src="images/client/06.jpg" class="avatar avatar-medium rounded-circle img-thumbnail" alt="">
                        <div class="flex-1 content ms-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark">Jack John</a></h5>
                            <small class="position text-muted">Professor</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="d-flex align-items-center">
                        <img src="images/client/01.jpg" class="avatar avatar-medium rounded-circle img-thumbnail" alt="">
                        <div class="flex-1 content ms-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark">Roger Jackson</a></h5>
                            <small class="position text-muted">Professor</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="d-flex align-items-center">
                        <img src="images/client/02.jpg" class="avatar avatar-medium rounded-circle img-thumbnail" alt="">
                        <div class="flex-1 content ms-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark">Luchhi Cina</a></h5>
                            <small class="position text-muted">Professor</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="d-flex align-items-center">
                        <img src="images/client/03.jpg" class="avatar avatar-medium rounded-circle img-thumbnail" alt="">
                        <div class="flex-1 content ms-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark">Sophiya Cally</a></h5>
                            <small class="position text-muted">Professor</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="d-flex align-items-center">
                        <img src="images/client/04.jpg" class="avatar avatar-medium rounded-circle img-thumbnail" alt="">
                        <div class="flex-1 content ms-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark">Johnny English</a></h5>
                            <small class="position text-muted">Professor</small>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
        <!-- Teachers End -->

        <!-- Testi Start -->
        <div class="container mt-100 mt-60">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title mb-4 pb-2">
                        <h4 class="title mb-4">What Students Say ?</h4>
                        <p class="text-muted para-desc mx-auto mb-0">Start working with <span class="text-primary fw-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row justify-content-center">
                <div class="col-lg-12 mt-4">
                    <div class="tiny-three-item">
                        <div class="tiny-slide">
                            <div class="d-flex client-testi m-2">
                                <img src="images/client/01.jpg" class="avatar avatar-small client-image rounded shadow" alt="">
                                <div class="flex-1 content p-3 shadow rounded bg-white position-relative">
                                    <ul class="list-unstyled mb-0">
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    </ul>
                                    <p class="text-muted mt-2">" It seems that only fragments of the original text remain in the Lorem Ipsum texts used today. "</p>
                                    <h6 class="text-primary">- Thomas Israel <small class="text-muted">C.E.O</small></h6>
                                </div>
                            </div>
                        </div>

                        <div class="tiny-slide">
                            <div class="d-flex client-testi m-2">
                                <img src="images/client/02.jpg" class="avatar avatar-small client-image rounded shadow" alt="">
                                <div class="flex-1 content p-3 shadow rounded bg-white position-relative">
                                    <ul class="list-unstyled mb-0">
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star-half text-warning"></i></li>
                                    </ul>
                                    <p class="text-muted mt-2">" One disadvantage of Lorum Ipsum is that in Latin certain letters appear more frequently than others. "</p>
                                    <h6 class="text-primary">- Barbara McIntosh <small class="text-muted">M.D</small></h6>
                                </div>
                            </div>
                        </div>

                        <div class="tiny-slide">
                            <div class="d-flex client-testi m-2">
                                <img src="images/client/03.jpg" class="avatar avatar-small client-image rounded shadow" alt="">
                                <div class="flex-1 content p-3 shadow rounded bg-white position-relative">
                                    <ul class="list-unstyled mb-0">
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    </ul>
                                    <p class="text-muted mt-2">" The most well-known dummy text is the 'Lorem Ipsum', which is said to have originated in the 16th century. "</p>
                                    <h6 class="text-primary">- Carl Oliver <small class="text-muted">P.A</small></h6>
                                </div>
                            </div>
                        </div>

                        <div class="tiny-slide">
                            <div class="d-flex client-testi m-2">
                                <img src="images/client/04.jpg" class="avatar avatar-small client-image rounded shadow" alt="">
                                <div class="flex-1 content p-3 shadow rounded bg-white position-relative">
                                    <ul class="list-unstyled mb-0">
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    </ul>
                                    <p class="text-muted mt-2">" According to most sources, Lorum Ipsum can be traced back to a text composed by Cicero. "</p>
                                    <h6 class="text-primary">- Christa Smith <small class="text-muted">Manager</small></h6>
                                </div>
                            </div>
                        </div>

                        <div class="tiny-slide">
                            <div class="d-flex client-testi m-2">
                                <img src="images/client/05.jpg" class="avatar avatar-small client-image rounded shadow" alt="">
                                <div class="flex-1 content p-3 shadow rounded bg-white position-relative">
                                    <ul class="list-unstyled mb-0">
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    </ul>
                                    <p class="text-muted mt-2">" There is now an abundance of readable dummy texts. These are usually used when a text is required. "</p>
                                    <h6 class="text-primary">- Dean Tolle <small class="text-muted">Developer</small></h6>
                                </div>
                            </div>
                        </div>

                        <div class="tiny-slide">
                            <div class="d-flex client-testi m-2">
                                <img src="images/client/06.jpg" class="avatar avatar-small client-image rounded shadow" alt="">
                                <div class="flex-1 content p-3 shadow rounded bg-white position-relative">
                                    <ul class="list-unstyled mb-0">
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    </ul>
                                    <p class="text-muted mt-2">" Thus, Lorem Ipsum has only limited suitability as a visual filler for German texts. "</p>
                                    <h6 class="text-primary">- Jill Webb <small class="text-muted">Designer</small></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
        <!-- Testi End -->


    </section><!--end section-->
    <!-- End -->


@endsection
