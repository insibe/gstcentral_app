<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/migrate', function(){
    \Artisan::call('migrate');
    dd('migrated!');
});


Route::get('/', function () {
    return view('welcome');
});

Route::get('/company/about-us', function () {
    return view('about');
});

Route::get('/company/investor-relations', function () {
    return view('investor-relations');
});

Route::get('/company/media-kit', function () {
    return view('media-kit');
});

Route::get('/company/careers', function () {
    return view('careers');
});

Route::get('/company/our-team', function () {
    return view('our-team');
});


Route::get('/legal/terms-of-service', function () {
    return view('terms-of-service');
});

Route::get('/legal/cookie-policy', function () {
    return view('cookie-policy');
});

Route::get('/legal/privacy-policy', function () {
    return view('privacy-policy');
});

Route::get('/legal/editorial-policy', function () {
    return view('editorial-policy');
});



Route::get('/pro', function () {
    return view('pro');
});

Route::get('/consult', function () {
    return view('consult.index');
});

Route::get('/community', function () {
    return view('community.index');
});



Route::get('/learn', function () {
    return view('learn.index');
});

Route::get('/resources', function () {
    return view('resources.index');
});

Route::get('/account', function () {
    return view('account.index');
})->middleware(['auth'])->name('dashboard');


Route::get('/verification', function () {
    return view('account.verification');
})->middleware(['auth'])->name('verification');





require __DIR__.'/auth.php';
